package test.com.cloudview.cloudview.repository

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import test.com.cloudview.cloudview.repository.models.ImageItemVk

@Database(entities = [(ImageItemVk::class)], version = 1)
abstract class ImagesDatabase : RoomDatabase() {

    abstract fun getDao(): ImagesDao

    companion object {
        private var database: ImagesDatabase? = null

        fun getInstance(context: Context): ImagesDatabase{
            if(database == null){
                synchronized(ImagesDatabase::class){
                    database = Room.databaseBuilder(context,
                            ImagesDatabase::class.java,
                            "imagesDatabase")
                            .allowMainThreadQueries()
                            .build()
                }
            }
            return database!!
        }
    }
}