package test.com.cloudview.cloudview.ui

import android.arch.lifecycle.Observer
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_image.*
import test.com.cloudview.cloudview.R
import test.com.cloudview.cloudview.adapters.TargetImageAdapter
import test.com.cloudview.cloudview.receiver.ConnectionLiveData
import test.com.cloudview.cloudview.repository.models.ImageItemVk
import java.lang.Exception

class ImageFragment : Fragment() {
    private var largeImgLoaded = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image, container, false)
    }

    /**
     * Берёт старое фото из кэша Retrofit.
     * После этого загружает новое изображение
     * Сделано для избавления от эффекта "моргания"
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.get()
                .load(arguments!!.getString(SMALL_IMG))
                .into(smallImgAdapter)

        loadLargeImages()

        ConnectionLiveData(activity!!.applicationContext).observe(this, Observer {
            if(it!! && !largeImgLoaded){
                loadLargeImages()
            }
        })
    }

    /**
     * Из-за более чем одного использования - вынесено в отдельный метод
     */
    private fun loadLargeImages(){
        Picasso.get()
                .load(arguments!!.getString(LARGE_IMG))
                .into(largeImgAdapter)
    }

    /**
     * Загружает небольшое изображение как preview.
     * Может быть ситуация, когда уже загружено большое. Тогда не заменяет
     */
    private val smallImgAdapter = object : TargetImageAdapter{
        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            if(!largeImgLoaded && iv_image != null){
                iv_image.setImageBitmap(bitmap!!)
            }
        }
    }

    /**
     * Загружает большо изображение, скрывает прогрессбар
     */
    private val largeImgAdapter = object : TargetImageAdapter{
        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            if(iv_image != null) {
                iv_image.setImageBitmap(bitmap!!)
                largeImgLoaded = true
                hideProgressBar()
            }
        }

        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
            super.onBitmapFailed(e, errorDrawable)
            hideProgressBar()
        }
    }

    private fun hideProgressBar() {
        if(progressBar != null) {
            progressBar.visibility = View.GONE
        }
    }

    companion object {
        const val SMALL_IMG = "small_img"
        const val LARGE_IMG = "large_img"

        /**
         * Добавляет в arguments ссылки на изображения
         */
        fun getInstance(vk: ImageItemVk): ImageFragment{
            val fragment = ImageFragment()
            val args = Bundle()
            args.putString(SMALL_IMG, vk.photo_130)
            args.putString(LARGE_IMG, vk.photo_604)
            fragment.arguments = args
            return fragment
        }
    }

}