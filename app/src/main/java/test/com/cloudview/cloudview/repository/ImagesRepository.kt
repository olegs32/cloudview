package test.com.cloudview.cloudview.repository

import android.arch.lifecycle.LiveData
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import test.com.cloudview.cloudview.ApplicationClass
import test.com.cloudview.cloudview.repository.models.HandlerResponse
import test.com.cloudview.cloudview.repository.models.ImageItemVk

object ImagesRepository {
    private val database = ImagesDatabase.getInstance(ApplicationClass.getInstance())
    private val retrofit = Retrofit.Builder()
            .baseUrl("https://api.vk.com/method/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    private val vkApi = retrofit.create(ImagesApi::class.java)
    private const val VK_GROUP = "-23065151"
    private const val LOG_TAG = "ImagesLog"

    /**
     * Проверяет есть ли изображения в базе. Если нет, делает запрос
     */
    fun getData() : LiveData<List<ImageItemVk>>{
        if(database.getDao().getCount() == 0){
            getImages()
        }
        return database.getDao().getAll()
    }

    private fun getImages(){
        vkApi.getImages(VK_GROUP, "wall")
                .enqueue(object : Callback<HandlerResponse>{
                    override fun onResponse(call: Call<HandlerResponse>?, response: Response<HandlerResponse>?) {
                        val vkResponse = response!!.body()?.response
                        if(vkResponse != null){
                            database.getDao().addAll(vkResponse.items)
                        }
                    }

                    override fun onFailure(call: Call<HandlerResponse>?, t: Throwable?) {
                        Log.e(LOG_TAG, t!!.message)
                    }

                })
    }

    fun getDataCount() = database.getDao().getCount()

    fun getItem(itemId: Int) = database.getDao().getItemById(itemId)

}