package test.com.cloudview.cloudview.adapters

import com.squareup.picasso.Callback
import java.lang.Exception

interface ImageLoadedCallback : Callback {

    override fun onError(e: Exception?) {}
    
}