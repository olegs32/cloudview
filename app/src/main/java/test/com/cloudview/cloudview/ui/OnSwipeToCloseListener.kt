package test.com.cloudview.cloudview.ui

interface OnSwipeToCloseListener{

    fun onSwipeToClose()
}