package test.com.cloudview.cloudview.repository.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ImageItemVk (

        @PrimaryKey
        val id: Int,

        val photo_75: String,
        val photo_130: String,
        val photo_604: String
)