package test.com.cloudview.cloudview

import android.app.Application
import android.content.Context
import android.view.WindowManager
import android.util.DisplayMetrics

class ApplicationClass : Application(){

    override fun onCreate() {
        super.onCreate()
        appContext = this
    }

    companion object {
        private lateinit var appContext: ApplicationClass

        fun getInstance() = appContext

        /**
         * Метод для получения ширины экрана. Заложен тут, так как может понадобитьс в любом
         * месте
         */
        fun getScreenWidth(): Int {
            val metrics = DisplayMetrics()
            val windowManager = appContext.getSystemService(Context.WINDOW_SERVICE)
                    as WindowManager
            windowManager.defaultDisplay.getMetrics(metrics)
            return metrics.widthPixels
        }
    }
}