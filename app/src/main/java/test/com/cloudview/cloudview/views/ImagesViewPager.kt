package test.com.cloudview.cloudview.views

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import test.com.cloudview.cloudview.ui.OnSwipeToCloseListener

class ImagesViewPager(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {

    private var gestureDetector: GestureDetector
    private var onSwipeToCloseListener: OnSwipeToCloseListener? = null

    /**
     * Инициализируется слушатель косаний, который определеяет,
     * хочет ли пользователь закрыть изображение
     */
    init {
        gestureDetector = GestureDetector(context, SwipeDetector())
    }

    fun setOnSwipeListener(onSwipeToCloseListener: OnSwipeToCloseListener){
        this.onSwipeToCloseListener = onSwipeToCloseListener
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if(gestureDetector.onTouchEvent(ev) &&
                onSwipeToCloseListener != null ){

            onSwipeToCloseListener!!.onSwipeToClose()
            return true
        }
        return super.dispatchTouchEvent(ev)
    }

    private inner class SwipeDetector : GestureDetector.SimpleOnGestureListener() {

        /**
         * Высчитывает, больше ли минимального значания проведено по оси Y
         * и меньше ли, чем возможная дистанция при желании обычного swipe
         */
        override fun onFling(e1: MotionEvent, e2: MotionEvent,
                             velocityX: Float, velocityY: Float): Boolean {
            return  (Math.abs(e2.y - e1.y) > Y_DIST_TO_CLOSE &&
                    Math.abs(e2.x - e1.x) < X_SWIPE_DISTANCE)
        }
    }

    companion object {
        private const val X_SWIPE_DISTANCE = 240
        private const val Y_DIST_TO_CLOSE = 240
    }
}