package test.com.cloudview.cloudview.repository.models

data class ResponseVk(
        val count: Int,
        val items: List<ImageItemVk>
)