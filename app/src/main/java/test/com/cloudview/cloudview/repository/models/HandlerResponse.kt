package test.com.cloudview.cloudview.repository.models

data class HandlerResponse(
        val error: ErrorVk?,
        val response: ResponseVk?
)