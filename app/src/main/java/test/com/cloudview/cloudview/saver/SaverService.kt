package test.com.cloudview.cloudview.saver

import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.IBinder
import android.widget.Toast
import com.squareup.picasso.Picasso
import test.com.cloudview.cloudview.R
import test.com.cloudview.cloudview.adapters.TargetImageAdapter
import test.com.cloudview.cloudview.repository.ImagesRepository
import java.lang.Exception
import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.FileNotFoundException


class SaverService : Service(){

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val targetImage = ImagesRepository.getItem(intent!!.getIntExtra(ITEM_ID, 0))
        Picasso.get().load(targetImage.photo_604).into(target)
        return super.onStartCommand(intent, flags, startId)
    }

    /**
     * Цель загрузки изображения. При удачном результате - сохраняет изображения
     * в папку Pictures/CloudView
     */
    private val target = object : TargetImageAdapter {
        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
            showToast(R.string.bad_connect)
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            if (!photoDir.exists()) {
                photoDir.mkdir()
            }
            println(photoDir.absolutePath)
            val saveImage = File(photoDir, "${System.nanoTime()}.jpg")
            try {
                val outputStream = FileOutputStream(saveImage)
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                outputStream.flush()
                outputStream.close()
                showToast(R.string.save_success)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                showToast(R.string.bad_connect)
            } catch (e: IOException) {
                e.printStackTrace()
                showToast(R.string.bad_connect)
            }


        }
    }

    private fun showToast(message: Int){
        Toast.makeText(applicationContext,
                getString(message),
                Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val ITEM_ID = "item_id"
        val photoDir = File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "CloudView")
    }
}