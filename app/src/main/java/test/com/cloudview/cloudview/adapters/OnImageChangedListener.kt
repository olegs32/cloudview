package test.com.cloudview.cloudview.adapters

import android.support.v4.view.ViewPager

interface OnImageChangedListener : ViewPager.OnPageChangeListener{
    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
}