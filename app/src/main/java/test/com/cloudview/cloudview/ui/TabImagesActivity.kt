package test.com.cloudview.cloudview.ui

import android.Manifest
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_tab.*
import test.com.cloudview.cloudview.R
import test.com.cloudview.cloudview.adapters.ImagesPagerAdapter
import test.com.cloudview.cloudview.adapters.OnImageChangedListener
import test.com.cloudview.cloudview.repository.ImagesRepository
import test.com.cloudview.cloudview.saver.SaverService

class TabImagesActivity : AppCompatActivity(), OnSwipeToCloseListener {
    private var count = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab)

        setSupportActionBar(toolbar)

        initViewPager()

        targetPosition = intent.getIntExtra(POSITION, 0)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_image, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == R.id.nav_save){
            checkAndSave()
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     * Подписывается на данные из базы данных. Инициализирует адаптер для viewPager
     * Переходи к нужной позиции
     */
    private fun initViewPager() {

        ImagesRepository.getData().observe(this, Observer {
            imageAdapter = ImagesPagerAdapter(ArrayList(it), supportFragmentManager)
            vp_images.adapter = imageAdapter
            vp_images.currentItem = targetPosition
            count = it!!.size
            setCount(targetPosition)
        })

        vp_images.addOnPageChangeListener(object : OnImageChangedListener{
            override fun onPageSelected(position: Int) {
                targetPosition = position
                setCount(position)
            }
        })

        vp_images.setOnSwipeListener(this)

    }

    override fun onSwipeToClose() {
        onBackPressed()
    }

    /**
     * Проверка на разршение для записи файлов, так как с 23 API это опасное разрешение.
     */
    private fun checkAndSave(){
        if (Build.VERSION.SDK_INT < 23) {
            saveImage()
        }
        else {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                saveImage()
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_CODE &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
            saveImage()
        }
    }

    private fun saveImage() {
        val intent = Intent(this, SaverService::class.java)
        intent.putExtra(SaverService.ITEM_ID,
                imageAdapter!!.getTargetItem(targetPosition).id)
        startService(intent)
    }

    private fun setCount(position: Int){
        title = String.format(getString(R.string.textCount), position+1, count)
    }

    companion object {
        const val POSITION = "position"
        private const val REQUEST_CODE = 1
        var targetPosition = 0
        private var imageAdapter: ImagesPagerAdapter? = null
    }
}