package test.com.cloudview.cloudview.repository

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import test.com.cloudview.cloudview.repository.models.HandlerResponse

interface ImagesApi{

    /**
     * Метод для получения изображений из групп Vk
     */
    @FormUrlEncoded
    @POST("photos.get")
    fun getImages(
            @Field("owner_id") ownerId: String,
            @Field("album_id") albumId: String,
            @Field("v") version: String = "5.74"): Call<HandlerResponse>
}