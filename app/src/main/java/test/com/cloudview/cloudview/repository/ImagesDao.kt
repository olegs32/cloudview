package test.com.cloudview.cloudview.repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import test.com.cloudview.cloudview.repository.models.ImageItemVk

@Dao
interface ImagesDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll(items: List<ImageItemVk>)

    @Query("SELECT * FROM imageitemvk")
    fun getAll(): LiveData<List<ImageItemVk>>

    @Query("SELECT COUNT(*) FROM imageitemvk")
    fun getCount(): Int

    @Query("SELEct * FROM imageitemvk WHERE id = :itemId")
    fun getItemById(itemId: Int): ImageItemVk
}