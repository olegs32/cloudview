package test.com.cloudview.cloudview.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import test.com.cloudview.cloudview.repository.models.ImageItemVk
import test.com.cloudview.cloudview.ui.ImageFragment

class ImagesPagerAdapter(private val data: ArrayList<ImageItemVk>, fm: FragmentManager?)
    : FragmentStatePagerAdapter(fm) {

    /**
     * @return новый фрагмент, с заложенным в него нужным элементом
     */
    override fun getItem(position: Int) = ImageFragment.getInstance(data[position])

    override fun getCount() = data.size

    fun getTargetItem(position: Int) = data[position]
}