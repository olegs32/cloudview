package test.com.cloudview.cloudview.adapters

import android.arch.lifecycle.Observer
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_image.view.*
import test.com.cloudview.cloudview.ApplicationClass
import test.com.cloudview.cloudview.R
import test.com.cloudview.cloudview.repository.models.ImageItemVk

/**
 * @param gridCount нужное колличество столбцов. В зависимости от этого
 * подготавливается высота для элементов
 */
class RecyclerImageAdapter(gridCount: Int,
                           private val onItemSelectListener: OnItemSelectListener) :
        RecyclerView.Adapter<RecyclerImageAdapter.ViewHolder>(),
        Observer<List<ImageItemVk>> {

    private val data = ArrayList<ImageItemVk>()

    /**
     * Высчитывает высоту для элемента в зависимости от ширины экрана
     */
    init {
        targetHeight = ApplicationClass.getScreenWidth()/gridCount
    }

    /**
     * При получениии данных - добавлет их в лист и обновляет
     */
    override fun onChanged(t: List<ImageItemVk>?) {
        data.addAll(ArrayList(t))
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_image, parent, false)
        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                targetHeight)
        val viewHolder = ViewHolder(view)
        view.layoutParams = layoutParams

        return viewHolder
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(data[holder.adapterPosition])
        holder.itemView.setOnClickListener {
            onItemSelectListener.onItemSelect(holder.adapterPosition)
        }
    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        private val loadedCallback = object : ImageLoadedCallback{
            override fun onSuccess() {
                if(itemView!!.pb_image != null) {
                    itemView.pb_image.visibility = View.GONE
                }
            }
        }

        fun onBind(imageItemVk: ImageItemVk) {
            Picasso.get()
                    .load(Uri.parse(imageItemVk.photo_130))
                    .into(itemView.iv_image, loadedCallback)
        }

    }

    companion object {
        private var targetHeight = 0
    }

}