package test.com.cloudview.cloudview.receiver

import android.arch.lifecycle.LiveData
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.content.IntentFilter
import android.net.NetworkInfo


class ConnectionLiveData(private val context: Context) : LiveData<Boolean>(){

    override fun onActive() {
        super.onActive()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(networkReceiver, filter)
    }

    override fun onInactive() {
        super.onInactive()
        context.unregisterReceiver(networkReceiver)
    }

    /**
     * Ресивер, слушающий изменения состояния интернета. Официально был deprecated,
     * но очень хорошо подходит для этой ситуации
     */
    private val networkReceiver = object : BroadcastReceiver(){

        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent != null){
                val activeNetwork = intent.extras
                        .get(ConnectivityManager.EXTRA_NETWORK_INFO) as NetworkInfo
                postValue(activeNetwork.isConnectedOrConnecting)
            }
        }

    }
}