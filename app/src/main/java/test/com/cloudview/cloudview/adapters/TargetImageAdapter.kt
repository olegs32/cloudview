package test.com.cloudview.cloudview.adapters

import android.graphics.drawable.Drawable
import com.squareup.picasso.Target
import java.lang.Exception

interface TargetImageAdapter : Target {

    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}

    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
}