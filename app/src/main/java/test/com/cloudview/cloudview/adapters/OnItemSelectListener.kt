package test.com.cloudview.cloudview.adapters

interface OnItemSelectListener {

    fun onItemSelect(position: Int)
}