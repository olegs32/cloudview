package test.com.cloudview.cloudview.ui

import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import test.com.cloudview.cloudview.R
import test.com.cloudview.cloudview.adapters.OnItemSelectListener
import test.com.cloudview.cloudview.adapters.RecyclerImageAdapter
import android.content.res.Configuration
import android.support.v7.widget.GridLayoutManager
import test.com.cloudview.cloudview.repository.ImagesRepository
import android.support.v4.app.ActivityOptionsCompat
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_image.*
import test.com.cloudview.cloudview.receiver.ConnectionLiveData


class MainActivity : AppCompatActivity(), OnItemSelectListener, Observer<Boolean> {
    private var toastWasShowed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()

        if(savedInstanceState != null){
            rv_images.scrollToPosition(savedInstanceState.getInt(RECYCLER_VIEW_POSITION))
        }

        ConnectionLiveData(applicationContext).observe(this, this)
    }

    /**
     * Слушает изменени интернета. Если интернета нет, то один раз показывает
     * уведомление об этом.
     * Если не были загружены изображения ранее - Показывает уведомление
     * об отсутствии интернета.
     * Если интернет появился - загружает изображения
     */
    override fun onChanged(hasConnection: Boolean?) {

        if(hasConnection!!){
            toastWasShowed = false
        } else {
            if(!toastWasShowed){
                showBadConnectToast()
                toastWasShowed = true
            }
        }

        if(!hasConnection && ImagesRepository.getDataCount() == 0){
            ll_badConnect.visibility = View.VISIBLE
        } else if(hasConnection && rvAdapter!!.itemCount == 0){
            rvAdapter = null
            initRecyclerView()
            ll_badConnect.visibility = View.GONE
        }
    }

    private fun showBadConnectToast() {
        val toast = Toast.makeText(this,
                getString(R.string.bad_connect),
                Toast.LENGTH_SHORT)
        toast.view.findViewById<TextView>(android.R.id.message)
                .textAlignment = View.TEXT_ALIGNMENT_CENTER
        toast.show()
    }

    /**
     * Сохраняет последний отображенный элемент для перерисовки экрана
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(RECYCLER_VIEW_POSITION,
                (rv_images.layoutManager as GridLayoutManager).findFirstVisibleItemPosition())
    }

    /**
     * Подсчитывает необходимое колличество столбцов, в зависимости от screen orientation
     * Инициализирует адаптер для RecyclerView
     */
    private fun initRecyclerView() {
        val gridCount = if (resources.configuration.orientation
                == Configuration.ORIENTATION_PORTRAIT) {
            PORTRAIT_GRID_COUNT
        } else {
            LANDSCAPE_GRID_COUNT
        }
        rv_images.layoutManager = GridLayoutManager(this, gridCount)

        if(rvAdapter == null){
            rvAdapter = RecyclerImageAdapter(gridCount, this)
            ImagesRepository.getData().observe(this, rvAdapter!!)
        }
        rv_images.adapter = rvAdapter
    }

    /**
     * Открывает TabActivity с изображениями, переходя на нужное
     */
    override fun onItemSelect(position: Int) {
        val intent = Intent(this, TabImagesActivity::class.java)
        intent.putExtra(TabImagesActivity.POSITION, position)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                iv_image as View, "profile")
        startActivity(intent, options.toBundle())
    }

    companion object {
        const val LANDSCAPE_GRID_COUNT = 5
        const val PORTRAIT_GRID_COUNT = 3
        const val RECYCLER_VIEW_POSITION = "rv_position"

        private var rvAdapter: RecyclerImageAdapter? = null
    }
}
